<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>Klik Login</name>
   <tag></tag>
   <elementGuidId>759a91dd-6660-4d24-962a-acb67e1c45ca</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>android.widget.TextView</value>
      <webElementGuid>fb49f4d3-d28d-49cc-8150-56e7d9811a74</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>index</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>e3649569-092f-46c4-94c3-d0f734d36164</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Klik link untuk login aplikasi</value>
      <webElementGuid>099d9291-8e4f-44ea-8aa1-32c1d820fc29</webElementGuid>
   </webElementProperties>
   <locator>//*[@class = 'android.widget.TextView' and @index = '0' and (contains(@text, 'Klik link untuk login aplikasi') or contains(., 'Klik link untuk login aplikasi'))]</locator>
   <locatorStrategy>ATTRIBUTES</locatorStrategy>
</MobileElementEntity>
